import { defineConfig } from 'cypress'
require('dotenv').config()

export default defineConfig({
    e2e: {
        baseUrl: process.env.BASE_URL,
        viewportWidth: 1260,
        viewportHeight: 900,
        setupNodeEvents(on, config) {
            require('@cypress/grep/src/plugin')(config)
            return config
        },
    },
    env: {
        administratorAccessToken: process.env.ADMINISTRATOR_ACCESS_TOKEN,
    },
})
