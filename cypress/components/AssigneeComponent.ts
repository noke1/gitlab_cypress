export class AssigneeComponent {
    expandAssignee(): this {
        cy.dataTestId('assignee-ids-dropdown-toggle').click()
        return this
    }

    searchFor(phrase: string): this {
        cy.dataQaSelector('dropdown_input_field').eq(0).type(phrase)
        return this
    }

    pickAtPosition(position: number): this {
        cy.dataQaSelector('dropdown_list_content')
            .find('li')
            .should(
                'have.length',
                1,
                'Assignees found after searching for them'
            )
            .find('a')
            .eq(position)
            .click()
        return this
    }

    getAssigneeName() {
        return cy.dataQaSelector('username')
    }
}
