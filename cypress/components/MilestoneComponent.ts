export class MilestoneComponent {
    expandMilestone(): this {
        cy.dataQaSelector('issuable_milestone_dropdown').click()
        return this
    }

    /**
     * Looks like the same components in different pages have different locators...
     * Therefore I created this method to search for the milestone name inside the Create the issue page
     * For the milestone search in Issue Details Page please use the other searchFor... method
     * @param milestoneTitle
     * @returns
     */
    searchForOnCreateIssuePage(milestoneTitle: string): this {
        cy.get(
            '[data-qa-selector="issuable_milestone_dropdown"] ul[role="menu"] input'
        ).type(milestoneTitle)
        return this
    }

    /**
     * Looks like the same components in different pages have different locators...
     * Therefore I created this method to search for the milestone name inside the Issue Details Page
     * For the milestone search in Create Issue Page please use the other searchFor... method
     * @param milestoneTitle
     * @returns
     */
    searchForOnIssueDetailsPage(milestoneTitle: string): MilestoneComponent {
        cy.get(
            '[data-qa-selector="milestone_block"] ul[role="menu"] input'
        ).type(milestoneTitle)
        return this
    }

    pickFirst(): void {
        cy.dataTestId('milestone-items').first().click()
    }

    getMilestones() {
        return cy.dataQaSelector('milestone_link')
    }
}
