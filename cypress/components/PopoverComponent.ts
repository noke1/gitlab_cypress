export class PopoverComponent {
    closePopoverRichText(): void {
        cy.dataTestId('rich-text-promo-popover')
            .find('button[data-testid="close-button"]')
            .click()
    }

    confirmEditNotification(): void {
        cy.get('.popover-body button[data-testid="confirm-button"]').click()
    }
}
