export class LabelComponent {
    expandLabels(): this {
        cy.dataQaSelector('issuable_label_dropdown').click()
        return this
    }

    searchFor(phrase: string): this {
        cy.dataQaSelector('dropdown_input_field')
            .eq(1)
            .type(phrase, { force: true })
        return this
    }

    pickLabelWithName(name: string): this {
        cy.contains(name).click()
        return this
    }

    getLabels() {
        return cy.get(
            '[data-testid="collapsed-content"] [data-qa-selector="selected_label_content"]'
        )
    }

    closeComponent(): void {
        cy.dataQaSelector('close_labels_dropdown_button').click()
    }
}
