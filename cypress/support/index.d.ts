export {}

declare global {
    namespace Cypress {
        interface Chainable<Subject> {
            /**
             * Custom command to select DOM element by data-qa-selector attribute.
             * @example cy.dataQaSelector('greeting')
             */
            dataQaSelector(value: string): Chainable<JQuery<HTMLElement>>

            /**
             * Custom command to select DOM element by data-qa-submenu-item attribute.
             * @example cy.dataQaSubmenu('greeting')
             */
            dataQaSubmenuItem(value: string): Chainable<JQuery<HTMLElement>>

            /**
             * Custom command to select DOM element by data-qa-panel-name attribute.
             * @example cy.dataQaSubmenu('greeting')
             */
            dataQaPanelName(value: string): Chainable<JQuery<HTMLElement>>

            /**
             * Custom command to select DOM element by data-track-label attribute.
             * @example cy.dataQaSubmenu('greeting')
             */
            dataTrackLabel(value: string): Chainable<JQuery<HTMLElement>>

            /**
             * Custom command to select DOM element by data-testid attribute.
             * @example cy.dataQaSubmenu('greeting')
             */
            dataTestId(value: string): Chainable<JQuery<HTMLElement>>

            /**
             * Removes the unnecessary \n strings from the text scraped from the gitlab website
             * @param value
             */
            getNormalizedText(): Chainable<JQuery<HTMLElement>>

            /**
             *
             * @param value
             */
            visitProject(username: string, projectname: string): void

            /**
             * Converts the given array into the array of strings. strings are the texts extracted from each element
             * @param value
             */
            toArrayOfText(): Chainable<string[]>

            /**
             * Method to visit the issue page, the problem is there is some gitlab misconfiguration becouse it redirects to example.gitlab.com instead of localhost for example. I don't want to dig into that thats wy workaround was provided
             */
            visitIssue(url: string): void
        }
    }
}
