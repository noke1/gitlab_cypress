Cypress.Commands.add('dataQaSelector', (value: string) => {
    return cy.get(`[data-qa-selector='${value}']`)
})

Cypress.Commands.add('dataQaSubmenuItem', (value: string) => {
    return cy.get(`[data-qa-submenu-item='${value}']`)
})

Cypress.Commands.add('dataQaPanelName', (value: string) => {
    return cy.get(`[data-qa-panel-name='${value}']`)
})

Cypress.Commands.add('dataTrackLabel', (value: string) => {
    return cy.get(`[data-track-label='${value}']`)
})

Cypress.Commands.add('dataTestId', (value: string) => {
    return cy.get(`[data-testid='${value}']`)
})
