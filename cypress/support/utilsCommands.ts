Cypress.Commands.add(
    'getNormalizedText',
    { prevSubject: true },
    ($subject: JQuery<HTMLElement>): Cypress.Chainable<JQuery<HTMLElement>> => {
        return cy
            .wrap($subject)
            .invoke('text')
            .invoke('replace', '&nbsp;', '')
            .invoke('replace', /\n/g, '')
            .invoke('replace', /\s\s+/g, ' ') //replace multiple spaces with one
            .invoke('trim')
    }
)

Cypress.Commands.add(
    'toArrayOfText',
    { prevSubject: true },
    ($elementsArray: JQuery<HTMLElement>): Cypress.Chainable<string[]> => {
        const textArray: string[] = Cypress.$.makeArray($elementsArray).map(
            (element) => element.innerText
        )
        return cy.wrap(textArray)
    }
)

Cypress.Commands.add(
    'visitProject',
    (username: string, projectname: string): void => {
        cy.visit(`/${username}/${projectname}`)
    }
)

Cypress.Commands.add('visitIssue', (url: string): void => {
    cy.visit(
        url.replace('http://gitlab.example.com:8929', Cypress.config('baseUrl'))
    )
})
