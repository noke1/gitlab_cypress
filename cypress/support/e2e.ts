import './selectorsCommands'
import './utilsCommands'

const registerCypressGrep = require('@cypress/grep')
registerCypressGrep()

/*
 * Ive encountered an error on the create project page - and this is an application error not the tests. To avoid failing the tests becouse of that error the below code was written.
 */
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    if (
        err.message.includes(
            "Cannot read properties of null (reading 'classList')"
        ) ||
        err.message.includes(
            "Cannot read properties of null (reading 'addEventListener')"
        )
    ) {
        return false
    }
})
