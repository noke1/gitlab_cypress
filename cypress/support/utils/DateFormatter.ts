export class DateFormatter {
    private dateToFormat: Date

    constructor(dateToFormat: Date) {
        this.dateToFormat = dateToFormat
    }

    getDateInGitlabFormat() {
        return this.dateToFormat.toISOString().slice(0, 10)
    }
}
