import { User } from '../models/User'
export class LoginPage {
    getSignInButton() {
        return cy.dataQaSelector('register_link')
    }

    goToRegister(): void {
        cy.dataQaSelector('register_link').click()
    }

    getRegisterConfirmationAlert() {
        return cy.dataQaSelector('flash_container').find('div.gl-alert-body')
    }

    as(user: User): void {
        cy.dataQaSelector('login_field').type(user.username)
        cy.dataQaSelector('password_field').type(user.password)
    }

    submit(): void {
        cy.dataQaSelector('sign_in_button').click()
    }
}
