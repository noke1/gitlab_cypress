export class LeftSideMenuPage {
    logout() {
        cy.dataTestId('sign_out_link').click()
    }
    expandAvatar() {
        cy.dataTestId('user_avatar_content').click()
    }

    goToMenuOption(optionName: LeftSideMenuOptions): void {
        cy.dataQaSubmenuItem(optionName).eq(0).click()
        cy.dataQaSelector('breadcrumb_current_link')
            .invoke('text')
            .invoke('replace', /\n/g, '')
            .should('equal', optionName)
    }
}

type LeftSideMenuOptions =
    | 'Projects'
    | 'Groups'
    | 'Issues'
    | 'To-Do List'
    | 'Milestones'
    | 'Snippets'
    | 'Activity'
    | 'Assigned'
    | 'Review requests'
