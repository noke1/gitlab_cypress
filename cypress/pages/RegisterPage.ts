import { faker } from '@faker-js/faker'

export class RegisterPage {
    fillForm(): void {
        cy.dataQaSelector('new_user_first_name_field').type('test')
        cy.dataQaSelector('new_user_last_name_field').type('test')
        cy.dataQaSelector('new_user_username_field').type(
            faker.internet.userName()
        )
        cy.dataQaSelector('new_user_email_field').type(faker.internet.email())
        cy.dataQaSelector('new_user_password_field').type(
            'IAusdoi1029ud128376@@4'
        )
    }

    submitFrom(): void {
        cy.dataQaSelector('new_user_register_button').click()
    }
}
