export class DashboardPage {
    getWelcomeMessage() {
        return cy.dataQaSelector('welcome_title_content')
    }

    getTileTitles() {
        return cy.get('div > a h3').should('have.length', 4)
    }

    goToCreateProject(): void {
        cy.dataQaSelector('new_project_button').click()
    }
}
