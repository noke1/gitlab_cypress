export class ProjectsMainPage {
    getProjectName() {
        return cy.dataQaSelector('project_name_content')
    }

    getAlerts() {
        return cy.get("[role='alert']")
    }
}
