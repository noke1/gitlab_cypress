export class IssuesMainPage {
    goToNewIssue() {
        cy.contains('span', 'New issue').click({ force: true })
    }
}
