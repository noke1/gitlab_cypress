import { LabelComponent } from '../../../components/LabelComponent'
import { MilestoneComponent } from '../../../components/MilestoneComponent'
import { AssigneeComponent } from '../../../components/AssigneeComponent'
import { DateFormatter } from '../../../support/utils/DateFormatter'
import { PopoverComponent } from '../../../components/PopoverComponent'

export class CreateNewIssuePage {
    withLabel(labelName: string) {
        const labelSearchComponent: LabelComponent = new LabelComponent()

        labelSearchComponent
            .expandLabels()
            .searchFor(labelName)
            .pickLabelWithName(labelName)
    }

    withDescription(description: string): void {
        cy.dataQaSelector('issuable_form_description_field').type(description)
    }

    withDueDateDelayInDays(days: number) {
        const dueDateWithDelay: Date = new Date(
            Date.now() +
                60 /*seconds*/ *
                    60 /*minutes*/ *
                    24 /*hours*/ *
                    1000 /*ms*/ *
                    days
        )
        const formattedDate: string = new DateFormatter(
            dueDateWithDelay
        ).getDateInGitlabFormat()
        cy.get('#issuable-due-date').type(`${formattedDate}`)
    }

    withMilestone(milestoneTitle: string) {
        const mileStoneSearchComponent: MilestoneComponent =
            new MilestoneComponent()

        mileStoneSearchComponent
            .expandMilestone()
            .searchForOnCreateIssuePage(milestoneTitle)
            .pickFirst()
    }

    withAssignee(phrase: string): void {
        const assigneeSearchComponent: AssigneeComponent =
            new AssigneeComponent()

        assigneeSearchComponent
            .expandAssignee()
            .searchFor(phrase)
            .pickAtPosition(0)
    }

    withType(issueType: IssueType) {
        cy.get('#dropdown-toggle-btn-39').click()
        cy.dataTestId(`listbox-item-${issueType.toLowerCase()}`).click()
    }

    create(): void {
        cy.dataQaSelector('issuable_create_button').click()
    }

    withTitle(title: string): void {
        const popoverComponent: PopoverComponent = new PopoverComponent()
        popoverComponent.closePopoverRichText()
        cy.dataQaSelector('issuable_form_title_field').type(title)
    }
}

type IssueType = 'Issue' | 'Incident'
