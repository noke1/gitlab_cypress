import { LabelComponent } from '../../../components/LabelComponent'
import { AssigneeComponent } from './../../../components/AssigneeComponent'
import { PopoverComponent } from '../../../components/PopoverComponent'
import { MilestoneComponent } from '../../../components/MilestoneComponent'

export class IssueDetailsPage {
    closePopoverRichText(): void {
        const popoverComponent: PopoverComponent = new PopoverComponent()
        popoverComponent.closePopoverRichText()
    }

    addCustomReaction(reactionName: string): void {
        cy.dataTestId('emoji-picker')
            .click()
            .find('input')
            .clear()
            .type(reactionName)
        cy.get(`gl-emoji[title='${reactionName}']`).click()
    }

    clickReaction(reactionName: string): void {
        cy.get(`gl-emoji[title='${reactionName}']`).click()
    }

    getCurrentReactions() {
        const reactions: Record<string, number> = {}
        return cy
            .dataTestId('award-button')
            .each(($reaction) => {
                const reactionName: string = Cypress.$($reaction)
                    .find('gl-emoji')
                    .attr('title')
                const reactionsCount: number = +Cypress.$($reaction)
                    .find('span.js-counter')
                    .text()
                reactions[reactionName] = reactionsCount
            })
            .then(() => {
                return reactions
            })
    }

    pickLabel(milestoneTitle: string): void {
        const mileStoneComponent: MilestoneComponent = new MilestoneComponent()
        mileStoneComponent
            .searchForOnIssueDetailsPage(milestoneTitle)
            .pickFirst()
    }

    editMilestone(): void {
        cy.get('button#milestone-edit').click()
    }

    editLabel(): void {
        cy.dataQaSelector('labels_block')
            .find('[data-testid="edit-button"]')
            .click()
    }

    addLabel(labelName: string): void {
        const labelComponent: LabelComponent = new LabelComponent()
        labelComponent.pickLabelWithName(labelName).closeComponent()
    }

    getCurrentLabels() {
        const labelComponent: LabelComponent = new LabelComponent()
        return labelComponent.getLabels()
    }

    getEditedLog() {
        return cy.get('small')
    }

    getActivities() {
        return cy.get('#notes-list li', { timeout: 7_000 })
    }

    changeDescription(newDescription: string): void {
        this.closePopoverRichText()
        cy.dataQaSelector('markdown_editor_form_field')
            .clear()
            .type(newDescription)
    }

    saveChanges(): void {
        cy.dataTestId('issuable-save-button').click()
    }

    edit(): void {
        const popoverComponent: PopoverComponent = new PopoverComponent()
        popoverComponent.confirmEditNotification()
        cy.dataQaSelector('close_issue_button').prev().click()
    }

    editAssignee(): void {
        cy.dataQaSelector('assignee_block_container')
            .find('[data-qa-selector="edit_link"]')
            .click()
    }

    setAssignee(username: string): void {
        const assigneeComponent: AssigneeComponent = new AssigneeComponent()
        assigneeComponent.searchFor(username).pickAtPosition(0)
    }

    getLinkedItemsRows() {
        return cy.get('[data-testid="related-issues-body"] li a')
    }

    createNewRelatedIssue() {
        this.expandActionsMenu()
        cy.contains('p', 'New related issue').click({ force: true })
    }

    getDueDate() {
        return cy.dataTestId('sidebar-date-value').invoke('text')
    }

    getLabels() {
        const labelComponent: LabelComponent = new LabelComponent()
        return labelComponent.getLabels()
    }

    getMilestones() {
        const milestoneComponent: MilestoneComponent = new MilestoneComponent()
        return milestoneComponent.getMilestones()
    }

    getDescription() {
        return cy.dataTestId('gfm-content')
    }

    getAuthor() {
        return cy.get('strong a:first-of-type span.author')
    }

    getIssueStatus() {
        return cy.dataTestId('issues-icon').next()
    }

    getIssueTitle() {
        return cy.dataTestId('issue-title')
    }

    getAssignee() {
        const assigneeComponent: AssigneeComponent = new AssigneeComponent()
        return assigneeComponent.getAssigneeName()
    }

    private expandActionsMenu(): void {
        cy.get('#new-actions-header-dropdown__BV_toggle_').click()
    }
}
