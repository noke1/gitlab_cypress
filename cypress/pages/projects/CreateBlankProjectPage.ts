import { ProjectVisibility } from '../../models/Project'

export class CreateBlankProjectPage {
    withReadMe(isReadmeNeeded: boolean) {
        cy.dataQaSelector('initialize_with_readme_checkbox')
            .eq(0)
            .then(($readme) => {
                const isChecked: boolean = $readme.is(':checked')

                if (
                    (isReadmeNeeded && !isChecked) ||
                    (!isReadmeNeeded && isChecked)
                ) {
                    cy.dataQaSelector('initialize_with_readme_checkbox')
                        .eq(0)
                        .parent()
                        .click()
                } else {
                    const action = isReadmeNeeded ? 'checked' : 'UN-checked'
                    cy.log(`Readme Checkbox ${action} already`)
                }
            })
    }

    create() {
        cy.dataQaSelector('project_create_button').eq(0).click()
    }

    /**
     * This one was not easy... The checkboxes that I wanted to click are overlapped by the label next to them.
     * So I search for the checkboxes, go to its parent (div), then search for the label that describes the checkbox (it is important becouse ONLY THE LABEL AREA is clickable), click label, and then comeback to the checkboxes to see if it is checked
     */
    withVisibility(projectVisibility: ProjectVisibility): void {
        cy.then(() => {
            if (projectVisibility === 'private') {
                cy.dataQaSelector('private_radio')
                    .eq(0)
                    .parent()
                    .find('label')
                    .click()
                cy.dataQaSelector('private_radio').eq(0).should('be.checked')
            } else if (projectVisibility === 'internal') {
                cy.dataQaSelector('internal_radio')
                    .eq(0)
                    .parent()
                    .find('label')
                    .click()
                cy.dataQaSelector('internal_radio').eq(0).should('be.checked')
            } else if (projectVisibility === 'public') {
                cy.dataQaSelector('public_radio')
                    .eq(0)
                    .parent()
                    .find('label')
                    .click()
                cy.dataQaSelector('public_radio').eq(0).should('be.checked')
            }
        })
    }

    withProjectName(projectName: string): void {
        cy.dataQaSelector('project_name').eq(0).type(projectName)
    }
}
