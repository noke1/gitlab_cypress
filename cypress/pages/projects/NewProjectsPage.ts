export class NewProjectsPage {
    goToProjectCreationType(creationType: creationType): void {
        const typesMapping = new Map<creationType, string>([
            ['Blank', 'blank_project'],
            ['Template', 'create_from_template'],
            ['Import', 'import_project'],
        ])
        cy.dataQaPanelName(typesMapping.get(creationType)).click()
        //Just a check if I am on a correct page after the click
        cy.url().should('satisfy', (url) =>
            url.endsWith(typesMapping.get(creationType))
        )
    }
}

type creationType = 'Blank' | 'Template' | 'Import'
