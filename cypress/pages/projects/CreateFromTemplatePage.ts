export class CreateFromTemplatePage {
    useTemplateOf(name: TemplateName) {
        cy.contains('strong', name)
            .parent()
            .should('have.class', 'description')
            .parent()
            .should(
                'have.attr',
                'data-qa-selector',
                'template_option_container'
            )
            .find('[data-qa-selector="use_template_button"]')
            .click()
    }
}

type TemplateName = 'Ruby on Rails' | 'Spring' | 'NodeJS Express'
