import { faker } from '@faker-js/faker'
import { User } from '../models/User'

function getRandomUser(): User {
    return new User(
        faker.person.fullName(),
        faker.internet.email(),
        faker.internet.userName(),
        faker.internet.password()
    )
}

function getRandomAdministrator(): User {
    return new User(
        faker.person.fullName(),
        faker.internet.email(),
        faker.internet.userName(),
        faker.internet.password(),
        true
    )
}

export const UserProvider = {
    getRandomUser,
    getRandomAdministrator,
}
