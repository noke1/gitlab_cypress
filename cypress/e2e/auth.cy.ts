import { LoginPage } from '../pages/LoginPage'
import { DashboardPage } from '../pages/DashboardPage'
import { LeftSideMenuPage } from '../pages/LeftSideMenuPage'

import { UserProvider as userProvider } from '../data/UserProvider'

import { User } from '../models/User'

import { UserResource as userResource } from '../api/UserResource'
import { LoginResource as loginResource } from '../api/LoginResource'

const login: LoginPage = new LoginPage()
const dashboard: DashboardPage = new DashboardPage()
const rightSideMenuPage: LeftSideMenuPage = new LeftSideMenuPage()

describe('User', () => {
    let user: User

    beforeEach(() => {
        user = userProvider.getRandomUser()
        userResource.createUser(user)
    })

    afterEach(() => {
        userResource
            .getUserByParameters({ username: user.username })
            .its('id')
            .then((userId) => {
                userResource.deleteUser(userId)
            })
    })

    it('Regular user can login with correct data', { tags: '@smoke' }, () => {
        cy.visit('/')
        login.as(user)
        login.submit()

        dashboard
            .getWelcomeMessage()
            .invoke('text')
            .invoke('replace', /\n/g, '')
            .should('equal', 'Welcome to GitLab')
        dashboard.getTileTitles().then(($tiles) => {
            const texts: string[] = Cypress.$.makeArray($tiles).map(
                (tile) => tile.innerText
            )
            expect(texts).to.have.members([
                'Create a project',
                'Create a group',
                'Explore public projects',
                'Learn more about GitLab',
            ])
        })
    })

    it('Regular user can logout successfully', () => {
        loginResource.login(user)
        cy.visit('/projects/new')
        rightSideMenuPage.expandAvatar()

        rightSideMenuPage.logout()

        cy.url().should(
            'satisfy',
            (url) => url.endsWith('/users/sign_in'),
            'The url should end with /users/sign_in to be sure user was redirected after the logout!!'
        )
        login.getSignInButton().should('be.visible')
    })
})

describe('Administrator', () => {
    let administrator: User

    beforeEach(() => {
        administrator = userProvider.getRandomAdministrator()
        userResource.createUser(administrator)
    })

    afterEach(() => {
        userResource
            .getUserByParameters({ username: administrator.username })
            .its('id')
            .then((userId) => {
                userResource.deleteUser(userId)
            })
    })

    it('Administrator can login with correct data', { tags: '@smoke' }, () => {
        cy.visit('/')
        login.as(administrator)
        login.submit()

        dashboard
            .getWelcomeMessage()
            .getNormalizedText()
            .should('equal', 'Welcome to GitLab')

        dashboard.getTileTitles().then(($tiles) => {
            const texts: string[] = Cypress.$.makeArray($tiles).map(
                (tile) => tile.innerText
            )
            expect(texts).to.have.members([
                'Configure GitLab',
                'Add people',
                'Create a project',
                'Create a group',
            ])
        })
    })

    it('Administrator can logout successfully', () => {
        loginResource.login(administrator)
        cy.visit('/projects/new')
        rightSideMenuPage.expandAvatar()

        rightSideMenuPage.logout()

        cy.url().should(
            'satisfy',
            (url) => url.endsWith('/users/sign_in'),
            'The url should end with /users/sign_in to be sure user was redirected after the logout!!'
        )
        login.getSignInButton().should('be.visible')
    })
})
