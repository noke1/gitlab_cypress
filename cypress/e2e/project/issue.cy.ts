import { LeftSideMenuPage } from '../../pages/LeftSideMenuPage'
import { IssuesMainPage } from '../../pages/projects/issues/IssuesMainPage'
import { CreateNewIssuePage } from '../../pages/projects/issues/CreateNewIssuePage'
import { IssueDetailsPage } from '../../pages/projects/issues/IssueDetailsPage'

import { UserProvider as userProvider } from '../../data/UserProvider'

import { User } from '../../models/User'
import { Project } from '../../models/Project'
import { PersonalAccessToken } from '../../models/PersonalAccessToken'

import { UserResource as userResource } from '../../api/UserResource'
import { ProjectResource as projectResource } from '../../api/ProjectResource'
import { LoginResource as loginResource } from '../../api/LoginResource'
import { IssuesResource as issuesResource } from '../../api/IssuesResource'
import { Milestone } from '../../models/Milestone'
import { Label } from '../../models/Label'
import { DateFormatter } from '../../support/utils/DateFormatter'
import { Issue } from '../../models/Issue'
import { Member } from '../../models/Member'

const leftSideMenu: LeftSideMenuPage = new LeftSideMenuPage()
const issuesMainPage: IssuesMainPage = new IssuesMainPage()
const createNewIssuePage: CreateNewIssuePage = new CreateNewIssuePage()
const issueDetailsPage: IssueDetailsPage = new IssueDetailsPage()

let user: User
let project: Project

describe('Create an issue', () => {
    beforeEach(() => {
        user = userProvider.getRandomUser()
        project = new Project({
            name: 'testproj12334',
            visibility: 'private',
            initialize_with_readme: false,
        })
        userResource.createUser(user).then(({ id }) => {
            userResource
                .createPersonalAccessTokenForUser(
                    new PersonalAccessToken({
                        user_id: id,
                        name: 'Token via api',
                        scopes: ['api'],
                    })
                )
                .then(({ body: { token } }) => {
                    projectResource.createProjectAsUser(token, project)
                    cy.wrap({ userToken: token }).as('userToken')
                })
        })
        loginResource.login(user)
        cy.visitProject(user.username, project.name)
    })

    it(
        'Regular user can create an issue from a project level with minimum required data',
        { tags: '@smoke' },
        () => {
            leftSideMenu.goToMenuOption('Issues')
            issuesMainPage.goToNewIssue()

            createNewIssuePage.withTitle('title test')
            createNewIssuePage.create()

            issueDetailsPage.getIssueTitle().should('have.text', 'title test')
            issueDetailsPage
                .getIssueStatus()
                .getNormalizedText()
                .should(
                    'equal',
                    'Open',
                    'The newly created issue should have status Open'
                )
            issueDetailsPage.getAuthor().should('have.text', user.name)
            cy.get('@userToken')
                .its('userToken')
                .then((tokenValue) => {
                    issuesResource
                        .getIssueByParameters(
                            { author_username: user.username },
                            tokenValue
                        )
                        .then((response) => {
                            expect(response[0]['type']).to.eq('ISSUE')
                        })
                })
        }
    )

    it('Regular user can create an incident from a project level with minimum required data', () => {
        leftSideMenu.goToMenuOption('Issues')
        issuesMainPage.goToNewIssue()

        createNewIssuePage.withTitle('title test')
        createNewIssuePage.withType('Incident')
        createNewIssuePage.create()

        issueDetailsPage.getIssueTitle().should('have.text', 'title test')
        issueDetailsPage
            .getIssueStatus()
            .getNormalizedText()
            .should(
                'equal',
                'Open',
                'The newly created incident should have status Open'
            )
        issueDetailsPage.getAuthor().should('have.text', user.name)
        cy.then(function () {
            const token: string = this.userToken.userToken
            issuesResource
                .getIssueByParameters({ author_username: user.username }, token)
                .then((response) => {
                    expect(response[0]['type']).to.eq('INCIDENT')
                })
        })
    })

    it('Regular user can create an issue with description, assignee, milestone, label, due date', () => {
        const dueDateOffset: number = 7
        cy.get('@userToken')
            .its('userToken')
            .then((tokenValue) => {
                userResource.getUserProjects(user.username).then((response) => {
                    const projectId: number = Number(response[0].id)
                    projectResource.createProjectMilestone(
                        projectId,
                        tokenValue,
                        new Milestone({
                            id: projectId,
                            title: 'Test Milestone',
                        })
                    )
                    projectResource.createProjectLabel(
                        projectId,
                        tokenValue,
                        new Label({
                            id: projectId,
                            name: 'Test Label',
                            color: '#FFAABB',
                        })
                    )
                })
            })
        leftSideMenu.goToMenuOption('Issues')
        issuesMainPage.goToNewIssue()

        createNewIssuePage.withTitle('Various settings')
        createNewIssuePage.withDescription('test description')
        createNewIssuePage.withAssignee(user.username)
        createNewIssuePage.withMilestone('Test Milestone')
        createNewIssuePage.withLabel('Test Label')
        createNewIssuePage.withDueDateDelayInDays(dueDateOffset)
        createNewIssuePage.create()

        issueDetailsPage
            .getDescription()
            .should('have.text', 'test description')
        issueDetailsPage.getAssignee().should('have.text', user.name)
        issueDetailsPage
            .getMilestones()
            .toArrayOfText()
            .should('deep.equal', ['Test Milestone'])
        issueDetailsPage
            .getLabels()
            .toArrayOfText()
            .should('deep.equal', ['Test Label'])
        issueDetailsPage.getDueDate().then((date) => {
            const dueDate: string = new DateFormatter(
                new Date(date)
            ).getDateInGitlabFormat()
            const dateWithOffset: string = new DateFormatter(
                new Date(
                    // adding  -1 becouse the timezone converts the date to a date + 1...
                    new Date().setDate(new Date().getDate() + dueDateOffset - 1)
                )
            ).getDateInGitlabFormat()

            expect(dueDate).to.equal(dateWithOffset)
        })
    })

    it('Regular user can create new related issue and linked items section has entry about that', () => {
        userResource
            .getUserProjects(user.username)
            .its('[0].id')
            .as('projectId')
        cy.then(function () {
            const token: string = this.userToken.userToken
            const projectId: number = Number(this.projectId)
            projectResource
                .createIssueAsUser(
                    projectId,
                    token,
                    new Issue({
                        id: projectId,
                        title: 'Issue created via api call',
                    })
                )
                .its('body.web_url')
                .then((url) => {
                    cy.visitIssue(url)
                })
        })

        issueDetailsPage.createNewRelatedIssue()
        createNewIssuePage.withTitle('Related issue test')
        createNewIssuePage.create()

        issueDetailsPage
            .getLinkedItemsRows()
            .toArrayOfText()
            .should(
                'deep.equal',
                ['Issue created via api call'],
                'Linked items table should hold entries with linked issues titles'
            )
    })
})

describe('Edit an issue', () => {
    beforeEach(() => {
        user = userProvider.getRandomUser()
        project = new Project({
            name: 'testproj12334',
            visibility: 'private',
            initialize_with_readme: false,
        })
        userResource.createUser(user).then(({ id }) => {
            userResource
                .createPersonalAccessTokenForUser(
                    new PersonalAccessToken({
                        user_id: id,
                        name: 'Token via api',
                        scopes: ['api'],
                    })
                )
                .then(({ body: { token } }) => {
                    projectResource.createProjectAsUser(token, project)
                    cy.wrap({ userToken: token }).as('userToken')
                })
        })
        loginResource.login(user)
        cy.visitProject(user.username, project.name)
    })

    it(
        'Creator of an issue can change its description',
        { tags: '@smoke' },
        () => {
            const description: string = 'Description changed'
            userResource
                .getUserProjects(user.username)
                .its('[0].id')
                .as('projectId')
            cy.then(function () {
                const token: string = this.userToken.userToken
                const projectId: number = Number(this.projectId)
                projectResource
                    .createIssueAsUser(
                        projectId,
                        token,
                        new Issue({
                            id: projectId,
                            title: 'API Call Issue',
                            description: 'Issue created via an api call',
                        })
                    )
                    .its('body.web_url')
                    .then((url) => {
                        cy.visitIssue(url)
                    })
            })

            issueDetailsPage.edit()
            issueDetailsPage.changeDescription(description)
            issueDetailsPage.saveChanges()

            issueDetailsPage.getDescription().should('have.text', description)
            issueDetailsPage
                .getEditedLog()
                .getNormalizedText()
                .should('equal', `Edited just now by ${user.name}`)
            issueDetailsPage
                .getActivities()
                .should('have.length', 1)
                .toArrayOfText()
                .spread((descriptionActivity) => {
                    expect(descriptionActivity).to.equal(
                        `${user.name} changed the description just now`
                    )
                })
        }
    )

    it('Creator of an issue can change its assignee', () => {
        const startAssignee: User = userProvider.getRandomUser()
        userResource.createUser(startAssignee).as('startAssignee')
        userResource
            .getUserProjects(user.username)
            .its('[0].id')
            .as('projectId')
        cy.then(function () {
            const token: string = this.userToken.userToken
            const projectId: number = Number(this.projectId)
            const startAssigneeId: number = this.startAssignee.id
            projectResource.addUserToProject(
                projectId,
                token,
                new Member({
                    id: projectId,
                    user_id: startAssigneeId,
                    access_level: 'DEVELOPER',
                })
            )
            projectResource
                .createIssueAsUser(
                    projectId,
                    token,
                    new Issue({
                        id: projectId,
                        title: 'API Call Issue',
                        description: 'Issue created via an api call',
                        assignee_id: startAssigneeId,
                    })
                )
                .its('body.web_url')
                .then((url) => {
                    cy.visitIssue(url)
                })
        })

        issueDetailsPage.editAssignee()
        issueDetailsPage.setAssignee(user.name)

        issueDetailsPage.getAssignee().should('have.text', user.name)
        issueDetailsPage
            .getActivities()
            .should('have.length', 2)
            .toArrayOfText()
            .spread((_, assigneeChangeActivity) => {
                expect(assigneeChangeActivity).to.equal(
                    `${user.name} assigned to @${user.username} and unassigned @${startAssignee.username} just now`
                )
            })
    })

    it('Creator of an issue can change its label', () => {
        userResource
            .getUserProjects(user.username)
            .its('[0].id')
            .as('projectId')
        cy.then(function () {
            const token: string = this.userToken.userToken
            const projectId: number = Number(this.projectId)
            projectResource.createProjectLabel(
                projectId,
                token,
                new Label({
                    id: projectId,
                    name: 'Test Label',
                    color: '#FFAABB',
                })
            )
            projectResource
                .createIssueAsUser(
                    projectId,
                    token,
                    new Issue({
                        id: projectId,
                        title: 'API Call Issue',
                        description: 'Issue created via an api call',
                    })
                )
                .its('body.web_url')
                .then((url) => {
                    cy.visitIssue(url)
                })
        })

        issueDetailsPage.editLabel()
        issueDetailsPage.addLabel('Test Label')
        issueDetailsPage
            .getCurrentLabels()
            .should('have.length', 1)
            .getNormalizedText()
            .should('equal', 'Test Label')
        issueDetailsPage
            .getActivities()
            .should('have.length', 1)
            .first()
            .getNormalizedText()
            .should('include', `${user.name} added Test Label label just now`)
    })

    it('Creator of an issue can change its milestone', () => {
        userResource
            .getUserProjects(user.username)
            .its('[0].id')
            .as('projectId')
        cy.then(function () {
            const token: string = this.userToken.userToken
            const projectId: number = Number(this.projectId)

            projectResource.createProjectMilestone(
                projectId,
                token,
                new Milestone({
                    id: projectId,
                    title: 'Test Milestone',
                })
            )
            projectResource
                .createIssueAsUser(
                    projectId,
                    token,
                    new Issue({
                        id: projectId,
                        title: 'API Call Issue',
                        description: 'Issue created via an api call',
                    })
                )
                .its('body.web_url')
                .then((url) => {
                    cy.visitIssue(url)
                })
        })

        issueDetailsPage.editMilestone()
        issueDetailsPage.pickLabel('Test Milestone')

        issueDetailsPage
            .getMilestones()
            .getNormalizedText()
            .should('equal', 'Test Milestone')
        issueDetailsPage
            .getActivities()
            .should('have.length', 1)
            .toArrayOfText()
            .spread((milestoneActivity) => {
                expect(milestoneActivity).to.equal(
                    `${user.name} changed milestone to %Test Milestone just now`
                )
            })
    })

    it.skip('Creator of an issue can change its due date', () => {})
    it.skip('Creator of an issue can change its time tracking (add time)', () => {})
    it.skip('Creator of an issue can add an attachment', () => {})
    it.skip('Creator of an issue can close it', () => {})

    it('Other user (non-creator) can upvote, downvote', () => {
        cy.intercept('GET', '**award_emoji**').as('getEmojis')
        userResource
            .getUserProjects(user.username)
            .its('[0].id')
            .as('projectId')
        cy.then(function () {
            const token: string = this.userToken.userToken
            const projectId: number = Number(this.projectId)

            projectResource
                .createIssueAsUser(
                    projectId,
                    token,
                    new Issue({
                        id: projectId,
                        title: 'API Call Issue',
                        description: 'Issue created via an api call',
                    })
                )
                .its('body.web_url')
                .then((url) => {
                    cy.visitIssue(url)
                })
        })

        cy.wait('@getEmojis')
        issueDetailsPage.getCurrentReactions().should('deep.equal', {
            'thumbs up sign': 0,
            'thumbs down sign': 0,
        })
        issueDetailsPage.clickReaction('thumbs up sign')
        issueDetailsPage.clickReaction('thumbs down sign')
        issueDetailsPage.getCurrentReactions().should('deep.equal', {
            'thumbs up sign': 1,
            'thumbs down sign': 1,
        })
        issueDetailsPage.clickReaction('thumbs up sign')
        issueDetailsPage.clickReaction('thumbs down sign')
        issueDetailsPage.getCurrentReactions().should('deep.equal', {
            'thumbs up sign': 0,
            'thumbs down sign': 0,
        })
    })

    it('Other user (non-creator) can add/delete custom reaction', () => {
        cy.intercept('GET', '**award_emoji**').as('getEmojis')
        userResource
            .getUserProjects(user.username)
            .its('[0].id')
            .as('projectId')
        cy.then(function () {
            const token: string = this.userToken.userToken
            const projectId: number = Number(this.projectId)

            projectResource
                .createIssueAsUser(
                    projectId,
                    token,
                    new Issue({
                        id: projectId,
                        title: 'API Call Issue',
                        description: 'Issue created via an api call',
                    })
                )
                .its('body.web_url')
                .then((url) => {
                    cy.visitIssue(url)
                })
        })

        cy.wait('@getEmojis')
        issueDetailsPage.getCurrentReactions().should('deep.equal', {
            'thumbs up sign': 0,
            'thumbs down sign': 0,
        })
        issueDetailsPage.addCustomReaction('baseball')
        issueDetailsPage.addCustomReaction('person doing cartwheel')
        issueDetailsPage.addCustomReaction('call me hand')
        issueDetailsPage.getCurrentReactions().should('deep.equal', {
            'thumbs up sign': 0,
            'thumbs down sign': 0,
            baseball: 1,
            'person doing cartwheel': 1,
            'call me hand': 1,
        })
        issueDetailsPage.clickReaction('baseball')
        issueDetailsPage.clickReaction('person doing cartwheel')
        issueDetailsPage.clickReaction('call me hand')
        issueDetailsPage.getCurrentReactions().should('deep.equal', {
            'thumbs up sign': 0,
            'thumbs down sign': 0,
        })
    })

    it.skip('Other user (non-creator) can post a comment in the issue', () => {})
    it.skip('Other user (non-creator) can add a new task to the issue', () => {})
    it.skip('Other user (non-creator) can add an existing task to the issue', () => {})
})

describe('Read an issue', () => {
    it.skip('Regular user can see created issue in the issue list', () => {})
})

describe('Delete an issue', () => {
    beforeEach(() => {
        user = userProvider.getRandomUser()
        project = new Project({
            name: 'testproj12334',
            visibility: 'private',
            initialize_with_readme: false,
        })
        userResource.createUser(user).then(({ id }) => {
            userResource
                .createPersonalAccessTokenForUser(
                    new PersonalAccessToken({
                        user_id: id,
                        name: 'Token via api',
                        scopes: ['api'],
                    })
                )
                .then(({ body: { token } }) => {
                    projectResource.createProjectAsUser(token, project)
                    cy.wrap({ userToken: token }).as('userToken')
                })
        })
        loginResource.login(user)
        cy.visitProject(user.username, project.name)
    })
    it.skip('Regular user who created the issue can delete it', () => {
        cy.then(function () {
            const token: string = this.userToken.userToken
            const projectId: number = Number(this.projectId)
            projectResource
                .createIssueAsUser(
                    projectId,
                    token,
                    new Issue({
                        id: projectId,
                        title: 'API Call Issue',
                        description: 'Issue created via an api call',
                    })
                )
                .its('body.web_url')
                .then((url) => {
                    cy.visitIssue(url)
                })
        })
    })
})
