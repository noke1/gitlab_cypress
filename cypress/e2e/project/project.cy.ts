import { ProjectsMainPage } from '../../pages/projects/ProjectsMainPage'
import { NewProjectsPage } from '../../pages/projects/NewProjectsPage'
import { CreateBlankProjectPage } from '../../pages/projects/CreateBlankProjectPage'
import { CreateFromTemplatePage } from '../../pages/projects/CreateFromTemplatePage'

import { UserProvider as userProvider } from '../../data/UserProvider'

import { User } from '../../models/User'
import { Project } from '../../models/Project'

import { UserResource as userResource } from '../../api/UserResource'
import { ProjectResource as projectResource } from '../../api/ProjectResource'
import { LoginResource as loginResource } from '../../api/LoginResource'

const projectsMainPage: ProjectsMainPage = new ProjectsMainPage()
const newProjectsPage: NewProjectsPage = new NewProjectsPage()
const createBlankProject: CreateBlankProjectPage = new CreateBlankProjectPage()
const createFromTemplate: CreateFromTemplatePage = new CreateFromTemplatePage()

describe('Create a blank project', () => {
    let user: User

    beforeEach(() => {
        user = userProvider.getRandomUser()
        userResource.createUser(user)
        loginResource.login(user)

        cy.visit('/projects/new')
    })

    afterEach(() => {
        projectResource.deleteUserProjects(user.username)
        userResource
            .getUserByParameters({ username: user.username })
            .its('id')
            .then((userId) => {
                userResource.deleteUser(userId)
            })
    })

    const projects: Project[] = [
        new Project({
            name: 'testproj12334',
            visibility: 'private',
            initialize_with_readme: false,
        }),
        new Project({
            name: 'testproj12334',
            visibility: 'public',
            initialize_with_readme: false,
        }),
        new Project({
            name: 'testproj12334',
            visibility: 'internal',
            initialize_with_readme: true,
        }),
    ]
    projects.forEach((projectDetails) => {
        it(
            `Regular user can create a ${projectDetails.visibility} blank project and readme: ${projectDetails.initialize_with_readme}`,
            { tags: '@smoke' },
            () => {
                newProjectsPage.goToProjectCreationType('Blank')

                createBlankProject.withProjectName(projectDetails.name)
                createBlankProject.withVisibility(projectDetails.visibility)
                createBlankProject.withReadMe(
                    projectDetails.initialize_with_readme
                )
                createBlankProject.create()

                projectsMainPage
                    .getProjectName()
                    .getNormalizedText()
                    .should('equal', projectDetails.name)
                projectsMainPage
                    .getAlerts()
                    .should('have.length.at.least', 1)
                    .then(($alerts) => {
                        const alertsText: string[] = Cypress.$.makeArray(
                            $alerts
                        ).map((alert) => alert.innerText)
                        expect(alertsText).to.include(
                            `Project \'${projectDetails.name}\' was successfully created.`
                        )
                    })
                userResource
                    .getUserProjects(user.username)
                    .its(0)
                    .its('name')
                    .should('equal', projectDetails.name)
            }
        )
    })
})

describe('Create a project from template', () => {
    let user: User
    const project: Project = new Project({
        name: 'testproj12334',
        visibility: 'private',
        initialize_with_readme: false,
    })

    beforeEach(() => {
        user = userProvider.getRandomUser()
        userResource.createUser(user)
        loginResource.login(user)

        cy.visit('/projects/new')
    })

    afterEach(() => {
        projectResource.deleteUserProjects(user.username)
        userResource
            .getUserByParameters({ username: user.username })
            .its('id')
            .then((userId) => {
                userResource.deleteUser(userId)
            })
    })

    it(`Regular user can create a project form template`, () => {
        cy.intercept({ method: 'POST', url: '/api/graphql' }).as(
            'createFromTemplate'
        )
        newProjectsPage.goToProjectCreationType('Template')
        createFromTemplate.useTemplateOf('Spring')

        createBlankProject.withProjectName(project.name)
        createBlankProject.withVisibility(project.visibility)
        createBlankProject.create()
        cy.wait('@createFromTemplate')

        projectsMainPage
            .getProjectName()
            .getNormalizedText()
            .should('equal', project.name)
        projectsMainPage
            .getAlerts()
            .should('have.length.at.least', 1)
            .then(($alerts) => {
                const alertsText: string[] = Cypress.$.makeArray($alerts).map(
                    (alert) => alert.innerText
                )
                expect(alertsText).to.include(
                    'The project was successfully imported.'
                )
            })
        userResource
            .getUserProjects(user.username)
            .its(0)
            .its('name')
            .should('equal', project.name)
    })
})

describe('Create a project from import', () => {
    let user: User
    const project: Project = new Project({
        name: 'testproj12334',
        visibility: 'private',
        initialize_with_readme: false,
    })

    beforeEach(() => {
        user = userProvider.getRandomUser()
        userResource.createUser(user)
        loginResource.login(user)

        cy.visit('/projects/new')
    })

    afterEach(() => {
        projectResource.deleteUserProjects(user.username)
        userResource
            .getUserByParameters({ username: user.username })
            .its('id')
            .then((userId) => {
                userResource.deleteUser(userId)
            })
    })

    it(`Regular user can create a project form import`, () => {
        newProjectsPage.goToProjectCreationType('Import')
        //Leaving that blank becouse there is no api to turn on the sources for import. So I want to skip to some more exciting tests
        cy.get('#import-project-pane h4').should(
            'have.text',
            'No import options available'
        )
    })
})
