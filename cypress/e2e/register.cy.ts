import { LoginPage } from '../pages/LoginPage'
import { RegisterPage } from '../pages/RegisterPage'

const login: LoginPage = new LoginPage()
const register: RegisterPage = new RegisterPage()

describe('Regular user registration', () => {
    beforeEach(() => {
        cy.visit('/')
    })

    it(
        'Can register with correct data - confirmation message is shown',
        { tags: '@smoke' },
        () => {
            login.goToRegister()

            register.fillForm()
            register.submitFrom()

            cy.url().should('include', '/users/sign_in#login-pane')
            login
                .getRegisterConfirmationAlert()
                .should('be.visible')
                .getNormalizedText()
                .should(
                    'equal',
                    'You have signed up successfully. However, we could not sign you in because your account is awaiting approval from your GitLab administrator.'
                )
        }
    )
})
