import { UserResource as userResource } from './UserResource'

import { User } from '../models/User'

/* This is not a resource at all... I did not find any api call to use for logging in, so some hacks need to be applied:
* 1. visit the gitlab page to get the hidden authenticity_token 
2. Send a request to /users/sign_in (not an api route) with the username, pass, authenticity_token 
3. Navigate to the portal
*/
function login({ username, password }: User): void {
    cy.request({
        method: 'GET',
        url: `/users/sign_in`,
        headers: {
            'content-type': 'application/json',
        },
    }).then(({ body }) => {
        const authenticity_token = Cypress.$(body)
            .find('input[name="authenticity_token"]')
            .val()
        userResource.signIn({
            username,
            password,
            remember_me: 0,
            authenticity_token,
        })
    })
}

export const LoginResource = {
    login,
}
