/**
 * Generic function that allows to query the issues with multiple parameters
 * @param parameters - a list of key/value pairs to query on
 * @returns a full api response with the list of issues matching the criteria
 */
function getIssueByParameters(
    parameters: Record<string, string>,
    userToken: string
) {
    const queryString: string = Object.entries(parameters)
        .map(
            ([key, value]) =>
                `${encodeURIComponent(key)}=${encodeURIComponent(value)}`
        )
        .join('&')

    cy.request({
        method: 'GET',
        url: `/api/v4/issues?${queryString}`,
        headers: {
            'private-token': userToken,
            'content-type': 'application/json',
        },
    }).then((response) => {
        expect(response.status).to.eq(200)
        expect(response.body).to.have.length.at.least(1)
        cy.wrap(response.body).as('issuesData')
    })

    return cy.get('@issuesData')
}

export const IssuesResource = {
    getIssueByParameters,
}
