import { PersonalAccessToken } from '../models/PersonalAccessToken'
import { User } from '../models/User'

export const UserResource = {
    createUser,
    getUserByParameters,
    deleteUser,
    getUserProjects,
    signIn,
    createPersonalAccessTokenForUser,
}

function createUser(user: User) {
    return cy
        .request({
            method: 'POST',
            url: '/api/v4/users',
            headers: {
                'private-token': Cypress.env('administratorAccessToken'),
                'content-type': 'application/json',
            },
            body: JSON.stringify(user),
        })
        .then((response) => {
            expect(response.status).to.eq(201)
            return response.body
        })
}
/**
 * Generic function that allows to query the users with multiple parameters
 * @param parameters - a list of key/value pairs to query on
 * @returns a full api response with the list of users matching the criteria
 */
function getUserByParameters(
    parameters: Record<string, string>
) {
    const queryString: string = Object.entries(parameters)
        .map(
            ([key, value]) =>
                `${encodeURIComponent(key)}=${encodeURIComponent(value)}`
        )
        .join('&')

    cy.request({
        method: 'GET',
        url: `/api/v4/users?${queryString}`,
        headers: {
            'private-token': Cypress.env('administratorAccessToken'),
            'content-type': 'application/json',
        },
    }).then((response) => {
        expect(response.status).to.eq(200)
        expect(response.body).to.have.length.at.least(1)
        cy.wrap(response.body[0]).as('userData')
    })

    return cy.get('@userData')
}

function deleteUser(userId: string) {
    return cy
        .request({
            method: 'DELETE',
            url: `/api/v4/users/${userId}`,
            headers: {
                'private-token': Cypress.env('administratorAccessToken'),
                'content-type': 'application/json',
            },
        })
        .its('status')
        .should('eq', 204)
}

/**
 * Api call to to get a list of projects that user owns
 * @param userName username
 * @returns Array of projects that user owns
 */
function getUserProjects(
    userName: string
) {
    getUserByParameters({ username: userName })
        .its('id')
        .then((userId) => {
            cy.log(`***Obtained an Id: ${userId} from username: ${userName}***`)
            cy.request({
                method: 'GET',
                url: `/api/v4/users/${userId}/projects`,
                headers: {
                    'private-token': Cypress.env('administratorAccessToken'),
                    'content-type': 'application/json',
                },
            }).then((response) => {
                expect(response.status).to.eq(200)
                cy.wrap(response.body).as('userProjects')
            })
        })

    return cy.get('@userProjects')
}

/**
 * Method to send a json object to authenticate. Keep in mind that we are NOT using an API Call, just a simple http request
 * @param param0 authentication data
 */
function signIn({ username, password, remember_me, authenticity_token }): void {
    cy.request({
        method: 'POST',
        url: `/users/sign_in`,
        form: true,
        headers: {
            'content-type': 'application/json',
        },
        body: {
            'user[login]': username,
            'user[password]': password,
            'user[remember_me]': remember_me,
            authenticity_token,
        },
    })
}

function createPersonalAccessTokenForUser(
    personalAccessToken: PersonalAccessToken
) {
    return cy.request({
        method: 'POST',
        url: `/api/v4/users/${personalAccessToken.user_id}/personal_access_tokens`,
        headers: {
            'content-type': 'application/json',
            'private-token': Cypress.env('administratorAccessToken'),
        },
        body: JSON.stringify(personalAccessToken),
    })
}
