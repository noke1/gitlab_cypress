import { Issue } from '../models/Issue'
import { Label } from '../models/Label'
import { Member } from '../models/Member'
import { Milestone } from '../models/Milestone'
import { Project } from '../models/Project'
import { UserResource as userResource } from './UserResource'

function getProject(projectId: string) {
    return cy
        .request({
            method: 'GET',
            url: `/api/v4/projects/${projectId}`,
            headers: {
                'private-token': Cypress.env('administratorAccessToken'),
                'content-type': 'application/json',
            },
        })
        .its('status')
        .should('eq', 201)
}

function deleteUserProjects(userName: string) {
    userResource.getUserProjects(userName).then((response) => {
        const projectIds: string[] = Cypress.$.makeArray(response).map(
            (project) => String(project.id)
        )
        cy.wrap(projectIds).each((id) => {
            cy.request({
                method: 'DELETE',
                url: `/api/v4/projects/${id}`,
                headers: {
                    'private-token': Cypress.env('administratorAccessToken'),
                    'content-type': 'application/json',
                },
            })
                .its('status')
                .should('eq', 202)
        })
    })
}

function createProjectAsUser(personalAccessToken: string, project: Project) {
    return cy.request({
        method: 'POST',
        url: `/api/v4/projects/`,
        headers: {
            'private-token': personalAccessToken,
            'content-type': 'application/json',
        },
        body: JSON.stringify(project),
    })
}

function createProjectMilestone(
    projectId: number,
    personalAccessToken: string,
    milestone: Milestone
) {
    return cy.request({
        method: 'POST',
        url: `/api/v4/projects/${projectId}/milestones`,
        headers: {
            'private-token': personalAccessToken,
            'content-type': 'application/json',
        },
        body: JSON.stringify(milestone),
    })
}

function createProjectLabel(
    projectId: number,
    personalAccessToken: string,
    label: Label
) {
    return cy.request({
        method: 'POST',
        url: `/api/v4/projects/${projectId}/labels`,
        headers: {
            'private-token': personalAccessToken,
            'content-type': 'application/json',
        },
        body: JSON.stringify(label),
    })
}

function createIssueAsUser(
    projectId: number,
    personalAccessToken: string,
    issue: Issue
) {
    return cy.request({
        method: 'POST',
        url: `/api/v4/projects/${projectId}/issues`,
        headers: {
            'private-token': personalAccessToken,
            'content-type': 'application/json',
        },
        body: JSON.stringify(issue),
    })
}

function addUserToProject(
    projectId: number,
    personalAccessToken: string,
    member: Member
) {
    return cy.request({
        method: 'POST',
        url: `/api/v4/projects/${projectId}/members`,
        headers: {
            'private-token': personalAccessToken,
            'content-type': 'application/json',
        },
        body: JSON.stringify(member),
    })
}

export const ProjectResource = {
    getProject,
    deleteUserProjects,
    createProjectAsUser,
    createProjectMilestone,
    createProjectLabel,
    createIssueAsUser,
    addUserToProject,
}
