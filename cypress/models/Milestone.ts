interface IMilestone {
    id: number
    title: string
    description?: string
    due_date?: string
    start_date?: string
}

export class Milestone {
    id: number
    title: string
    description?: string
    due_date?: string
    start_date?: string

    constructor(milestone: IMilestone) {
        this.id = milestone.id
        this.title = milestone.title
        this.description = milestone.description
        this.due_date = milestone.due_date
        this.start_date = milestone.start_date
    }
}
