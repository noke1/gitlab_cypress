interface IPersonalAccessToken {
    user_id: number
    name: string
    expires_at?: Date
    scopes: string[]
}

export class PersonalAccessToken {
    user_id: number
    name: string
    expires_at?: Date
    scopes: string[]

    constructor(token: IPersonalAccessToken) {
        this.user_id = token.user_id
        this.name = token.name
        this.expires_at = token.expires_at
        this.scopes = token.scopes
    }
}
