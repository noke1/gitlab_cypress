interface ILabel {
    id: number
    name: string
    color: string
    description?: string
    priority?: string
}

export class Label {
    id: number
    name: string
    color: string
    description?: string
    priority?: string

    constructor(label: ILabel) {
        this.id = label.id
        this.name = label.name
        this.color = label.color
        this.description = label.description
        this.priority = label.priority
    }
}
