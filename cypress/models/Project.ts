interface IProjectVisbility {
    name: string
    visibility: ProjectVisibility
    initialize_with_readme: boolean
}

export type ProjectVisibility = 'private' | 'public' | 'internal'

export class Project {
    name: string
    visibility: ProjectVisibility
    initialize_with_readme: boolean

    constructor(project: IProjectVisbility) {
        this.name = project.name
        this.visibility = project.visibility
        this.initialize_with_readme = project.initialize_with_readme
    }
}
