export class User {
    name: string
    email: string
    username: string
    password: string
    admin: boolean = false

    constructor(
        name: string,
        email: string,
        username: string,
        password: string,
        admin: boolean = false
    ) {
        this.email = email
        this.name = name
        this.username = username
        this.password = password
        this.admin = admin
    }
}
