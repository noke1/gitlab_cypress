//Model taken from https://docs.gitlab.com/ee/api/members.html

type AccessLevelName =
    | 'NO_ACCESS'
    | 'MINIMAL_ACCESS'
    | 'GUEST'
    | 'REPORTER'
    | 'DEVELOPER'
    | 'MAINTAINER'
    | 'OWNER'

const accessLevel: Record<AccessLevelName, number> & Readonly<{}> = {
    NO_ACCESS: 0,
    MINIMAL_ACCESS: 5,
    GUEST: 10,
    REPORTER: 20,
    DEVELOPER: 30,
    MAINTAINER: 40,
    OWNER: 50,
}

interface IMember {
    //project id
    id: number
    user_id: number
    access_level: string
}

export class Member {
    id: number
    user_id: number
    access_level: number

    constructor(member: IMember) {
        this.id = member.id
        this.user_id = member.user_id
        this.access_level = accessLevel[member.access_level]
    }
}
