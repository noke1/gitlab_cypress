interface IIssue {
    //project id
    id: number
    title: string
    description?: string
    assignee_id?: number
}

export class Issue {
    id: number
    title: string
    description?: string
    assignee_id?: number

    constructor(issue: IIssue) {
        this.id = issue.id
        this.title = issue.title
        this.description = issue.description
        this.assignee_id = issue.assignee_id
    }
}
