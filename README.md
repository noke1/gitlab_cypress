# App under test

I picked the [Gitlab](https://docs.gitlab.com/ee/install/docker.html) web app as an app to be tested. My goal was to find an app that provides a structured UI layer as well as an rich API layer. Gitlab was a perfect candidate imo, but there is a one catch. Running the app locally consumes much PC usage so writing the Cypress tests became really annoying quickly. So I decided to move the Gitlab into the cloud server and I did that. The problem is that I don't want to spend much money on that so after 1 month free the hosting ended :)

# Testing framework

-   I tried to follow the Page Object Pattern
    -   Although the more tests I wrote the more I think that POP do not fit cypress at all
    -   Still I think POP creates a nice visibility of the pages that you operate on. I was able to locate the page just by reading the code. I value the readability the most!
    -   Pages are located inside the `pages` folder. Each class represents the `page`. I mean the page is just a part of a screen that I think is big enough to extract it as a page. There is also another concept which I called `component` and it is a quite small, repeatable modal/pop-up/whatever that was not big enough to abstract it as a page. I think it will solve its purpose as long as Gitlab crew use the same locator for the same buttons or fields!!!!
-   TypeScript was used so I can get familiar with it - TS looks fun
-   The tests that were automated were a complete freestyle. I mean I just wanted to practice crazy scenarios and Cypress interactions to gain as much knowledge as I can
-   You can spot some mixed approaches here like:
    -   classes and functions
    -   cypress commands and functions/providers
    -   and this is again to practice various approaches to the same problems
-   There is also some CI/CD file generated so the tests are run after the push to the remote, on the 3 browsers at the same time. It is possible becouse the tests are written in the correct way so there are no dependencies between them :)

# How to run the app under test

The process is about how to run the app in a Linux environment.

1.  Install a `docker` and `docker-compose` inside the machine that you want to install Gitlab on
2.  Create a folder and a `docker-compose.yml` file inside it. Paste the following content inside the created file:

```
version: '3.6'
services:
  web:
    image: 'gitlab/gitlab-ce:latest'
    restart: always
    hostname: 'gitlab.example.com'
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        external_url 'http://gitlab.example.com:8929'
        gitlab_rails['gitlab_shell_ssh_port'] = 2224
    ports:
      - '8929:8929'
      - '2224:22'
    volumes:
      - '$GITLAB_HOME/config:/etc/gitlab'
      - '$GITLAB_HOME/logs:/var/log/gitlab'
      - '$GITLAB_HOME/data:/var/opt/gitlab'
    shm_size: '256m'
```

Notice the app will be hosted on port `8929`

3. From the terminal navigate to the place where the `docker-compose.yml` file is stored and run:

```
docker-compose up -d
```

Wait until all the chunks are downloaded and the gitlab container is run.

4. The Gitlab app should be accessible on `localhost:8929`

# How to run the tests

You need to have the `nvm, npm` installed on the machine where you want the tests to be run. (Cypress - Getting started)[https://docs.cypress.io/guides/getting-started/installing-cypress] might be useful

1. If you have the docker container running connect to it by running

```
docker exec -it <container id> /bin/bash
```

2. Change the root user password by running:

```
sudo gitlab-rake "gitlab:password:reset[root]"
```

And then follow the instructions to change the password.

If there is any problem with that use [Gitlab docs - Password reset](https://docs.gitlab.com/ee/security/reset_user_password.html)

3. Now visit the `localhost:8989` (or the address where the gitlab is hosted) and login with the changed credentials

4. Generate a Administrator Access Token by following the: [Gitlab docs - Access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) Copy the access token becouse it will be needed soon

5. Clone the Cypress repository into the local machine

6. Inside the root path create a file `.env` with the following content inside:

```
BASE_URL='http://localhost:8929'
ADMINISTRATOR_ACCESS_TOKEN='<access token from step 4>'
```

7. Inside the terminal go to the Cypress repository root path and run

```
npm ci
```

8. Inside the terminal go to the Cypress repository run the following command to open the Cypress app:

```
npx cypress open
```

or run any of the test commands from `package.json` to run the tests
